SCRIPTS_DIR=$(realpath "$(pwd)")
PROJECT_DIR=$(realpath "${SCRIPTS_DIR}/../..")
DEVELOPER_DIR="${PROJECT_DIR}/developer"
BUILD_SCRIPTS_DIR="${DEVELOPER_DIR}/building"
EASYANSI_COLORPACKS_DIR="${PROJECT_DIR}/easyansi"
TESTS_DIR="${PROJECT_DIR}/tests"
DEMOS_DIR="${PROJECT_DIR}/demos"

PYPI_CREDENTIALS_DIR="/data/credentials/pypi"
CONDA_ENV="easyansi_env"


function show_common_global_variables() {
  echo "Main Proj Dir:         ${PROJECT_DIR}"
  echo "Color Packs Src Dir:   ${EASYANSI_COLORPACKS_DIR}"
  echo "Color Packs Tests Dir: ${TESTS_DIR}"
  echo "Developer Dir:         ${DEVELOPER_DIR}"
  echo "Scripts Dir:           ${SCRIPTS_DIR}"
  echo "Build Scripts Dir:     ${BUILD_SCRIPTS_DIR}"
  echo "Demos Dir:             ${DEMOS_DIR}"
  echo "PyPI Credentials Dir:  ${PYPI_CREDENTIALS_DIR}"
  echo "Conda Env:             ${CONDA_ENV}"
  echo ""
}


function show_heading() {
  local heading_text="${1}"
  local heading_length=70
  local heading_line=""
  for ((i=1; i<=heading_length; i++));
  do
    heading_line="${heading_line}="
  done
  echo "${heading_line}"
  echo "${heading_text}"
  echo "${heading_line}"
  echo
}


function activate_conda_environment() {
  local current_env
  # shellcheck disable=SC2063
  current_env="$(conda env list|grep '*'|cut -f1 -d\ )"
  echo "Current Conda Environment: ${current_env}"
  if [ "${current_env}" != "${CONDA_ENV}" ]
  then
    source activate "${CONDA_ENV}"
    # shellcheck disable=SC2063
    current_env="$(conda env list|grep '*'|cut -f1 -d\ )"
    echo "New Conda Environment: ${current_env}"
  fi
  echo ""
}
