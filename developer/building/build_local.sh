#!/usr/bin/env bash
set -e

# pip install build, twine

source ../common/common.sh
activate_conda_environment


function build_local() {
  show_heading "Building Easy ANSI Color Packs Locally"
  echo "Project dir: ${PROJECT_DIR}"
  echo ""
  cd "${PROJECT_DIR}"

  # Source distribution
  show_heading "Build Source Distribution"
  python -m build --sdist

  # Wheel distribution
  show_heading "Build Wheel Distribution"
  python -m build --wheel

  # Have twine check for common PyPI issues
  show_heading "Twine check for issues"
  twine check dist/*
}


build_local
