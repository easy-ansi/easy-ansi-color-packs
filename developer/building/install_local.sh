#!/usr/bin/env bash
set -e

source ../common/common.sh
activate_conda_environment


function install_local() {
  show_heading "Installing Easy ANSI Color Packs Locally"
  echo "Project dir: ${PROJECT_DIR}"
  echo ""
  cd "${PROJECT_DIR}"
  python -m pip install --editable .
}


install_local
