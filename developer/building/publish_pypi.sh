#!/usr/bin/env bash
set -e

PUBLISH_ENVIRONMENT=""
PUBLISH_TOKEN_FILE=""

if [ "$#" -ne 1 ]
then
    echo "USAGE: publish_pypi.sh (test/PRODUCTION)"
    exit 1
fi
PUBLISH_ENVIRONMENT="${1}"


source ../common/common.sh
activate_conda_environment


function publish_to_pypi() {
    show_heading "Publish Easy ANSI Color Packs to PyPI"
    echo "Project dir: ${PROJECT_DIR}"
    verify_publish_environment
    verify_publish_token
    push_to_pypi
}


function verify_publish_environment() {
    if [ ! "${PUBLISH_ENVIRONMENT}" == "test" ] && [ ! "${PUBLISH_ENVIRONMENT}" == "PRODUCTION" ]
    then
        echo "ERROR: Publishing environment must be 'test' or 'PRODUCTION'"
        echo "       Value provided: ${PUBLISH_ENVIRONMENT}"
        exit 1
    else
        echo "PyPI Environment: ${PUBLISH_ENVIRONMENT}"
    fi
}


function verify_publish_token() {
    PUBLISH_TOKEN_FILE="${PYPI_CREDENTIALS_DIR}/${PUBLISH_ENVIRONMENT}_pypi_easyansi_token.txt"
    if [ ! -f "${PUBLISH_TOKEN_FILE}" ]
    then
        echo "ERROR: PyPI token file not found: ${PUBLISH_TOKEN_FILE}"
        exit 1
    else
        echo "PyPI Token File: ${PUBLISH_TOKEN_FILE}"
    fi
}


function push_to_pypi() {
    echo "Pushing to PyPI Environment: ${PUBLISH_ENVIRONMENT}"
    cd "${PROJECT_DIR}"
    local pypi_repository
    local publish_token
    if [ "${PUBLISH_ENVIRONMENT}" == "test" ]
    then
        pypi_repository="testpypi"
    elif [ "${PUBLISH_ENVIRONMENT}" == "PRODUCTION" ]
    then
        pypi_repository="pypi"
    else
        pypi_repository="UNKNOWN"
    fi
    publish_token=$(cat "${PUBLISH_TOKEN_FILE}")
    twine upload \
        --repository "${pypi_repository}" \
        --disable-progress-bar \
        --username "joeysbytes" \
        dist/*
    publish_token=""
}


publish_to_pypi
